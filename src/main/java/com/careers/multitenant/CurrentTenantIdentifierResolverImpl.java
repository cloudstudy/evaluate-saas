package com.careers.multitenant;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.careers.multitenant.constants.CustomRequestAttributes;


@Component
public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {
    
     @Override
     public String resolveCurrentTenantIdentifier() {
    	 RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
         if (requestAttributes != null) {
              String identifier = (String) requestAttributes.getAttribute(CustomRequestAttributes.CURRENT_TENANT_IDENTIFIER,RequestAttributes.SCOPE_REQUEST);
              if (identifier != null) {
                  return identifier;
              }
         }
         return CustomRequestAttributes.DEFAULT_TENANT_ID;
     }
    
     @Override
     public boolean validateExistingCurrentSessions() {
         return true;
     }
}
