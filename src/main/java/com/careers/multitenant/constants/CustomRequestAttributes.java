package com.careers.multitenant.constants;

public interface CustomRequestAttributes {

	public static final String CURRENT_TENANT_IDENTIFIER = "CURRENT_TENANT_IDENTIFIER";
	public static final String DEFAULT_TENANT_ID = "skcc";

}
