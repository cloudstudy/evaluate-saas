package com.careers.evaluate.post.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Post {
	
	@Id
	@GeneratedValue
	private Long id;

	private String title;
		
	private String explanation;
	
	public Post() {
		
	}	
	
	public Post(String title, String explanation) {
		super();
		this.title = title;
		this.explanation = explanation;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", title=" + title + ", explanation=" + explanation + "]";
	}
	
	public void update(Post updatePost) {
		this.title =updatePost.title;
		this.explanation =updatePost.explanation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
}
