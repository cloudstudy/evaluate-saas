package com.careers.evaluate.post.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.careers.evaluate.post.domain.Post;
import com.careers.evaluate.post.repository.PostRepository;

@Controller
@RequestMapping("/{tenantId}/posts")
public class PostController {
	@Autowired
	private PostRepository postRepository;

	@GetMapping("")
	public String list(@PathVariable String tenantId, Model model, HttpSession session) {
		session.setAttribute("tenant", tenantId);
		model.addAttribute("posts", postRepository.findAll());
		return "/post/list";
	}

	@GetMapping("/form")
	public String form() {
		return "/post/form";
	}

	@PostMapping("")
	public String create(@PathVariable String tenantId, String title, String explanation) {
		Post post = new Post(title, explanation);
		System.out.println("create : " + post);
		postRepository.save(post);
		return "redirect:/" + tenantId + "/posts";
	}

	@GetMapping("/{id}/form")
	public String updateForm(@PathVariable Long id, Model model) {
		Post post = postRepository.findOne(id);
		System.out.println(post);
		model.addAttribute("posts", post);
		return "/post/updateform";
	}

	@PostMapping("/{id}")
	public String update(@PathVariable String tenantId, @PathVariable Long id, Post updatePost) {
		System.out.println("create : " + updatePost);
		Post post = postRepository.findOne(id);
		post.update(updatePost);
		System.out.println(post);
		postRepository.save(post);
		return "redirect:/" + tenantId + "/posts";
	}
}
