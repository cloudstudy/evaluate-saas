package com.careers.evaluate.post.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.careers.evaluate.post.repository.PostRepository;
import com.careers.multitenant.domain.MultitenancyConfigurationProperties;
import com.careers.multitenant.domain.Tenant;

@Controller
@RequestMapping("/{tenantId}/boards")
public class BoardController {
	
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private MultitenancyConfigurationProperties multitenancyConfigurationProperties;
	
	@GetMapping("")
	public String list(@PathVariable String tenantId, Model model, HttpSession session) {
		Tenant tenant = multitenancyConfigurationProperties.getTenants().stream().filter(x -> x.getId().equals(tenantId)).findFirst().get();
		
		session.setAttribute("tenant", tenant);
		model.addAttribute("posts", postRepository.findAll());
		return "/board/list";
	}
}
