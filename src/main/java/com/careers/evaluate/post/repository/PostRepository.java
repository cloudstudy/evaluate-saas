package com.careers.evaluate.post.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careers.evaluate.post.domain.Post;

public interface PostRepository extends JpaRepository<Post, Long> {

}
