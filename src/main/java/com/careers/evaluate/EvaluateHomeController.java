package com.careers.evaluate;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.careers.multitenant.domain.MultitenancyConfigurationProperties;
import com.careers.multitenant.domain.Tenant;

@Controller
@RequestMapping("/{tenantId}")
public class EvaluateHomeController {
	
	@Autowired
	private MultitenancyConfigurationProperties multitenancyConfigurationProperties;
	
	@GetMapping("")
	public String home(@PathVariable String tenantId, HttpSession session) {
		Tenant tenant = multitenancyConfigurationProperties.getTenants().stream().filter(x -> x.getId().equals(tenantId)).findFirst().get();
		session.setAttribute("tenant", tenant);
		return "/tenant/login";
	}
}
