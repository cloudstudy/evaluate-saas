package com.careers.evaluate.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careers.evaluate.user.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
