package com.careers.evaluate.user.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.careers.evaluate.user.domain.User;
import com.careers.evaluate.user.repository.UserRepository;

@Controller
@RequestMapping("/{tenantId}/users")
public class UserController {
	@Autowired
	private UserRepository userRepository;

	@GetMapping("")
	public String list(@PathVariable String tenantId, Model model, HttpSession session) {
		session.setAttribute("tenant", tenantId);
		System.out.println(userRepository.findAll());
		model.addAttribute("users", userRepository.findAll());
		return "/user/list";
	}

	@GetMapping("/form")
	public String form() {
		return "/user/form";
	}

	@PostMapping("")
	public String Create(@PathVariable String tenantId, String userId, String password, String name, String email) {
		User user = new User(userId, password, name, tenantId, email);
		userRepository.save(user);
		return "redirect:/" + tenantId + "/users";
	}
}
