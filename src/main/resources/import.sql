CREATE TABLE USER (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  email varchar(45) DEFAULT NULL,
  name varchar(45) DEFAULT NULL,
  password varchar(45) DEFAULT NULL,
  user_id varchar(45) DEFAULT NULL
);

INSERT INTO USER (USER_ID, PASSWORD, NAME, EMAIL) VALUES('test1', '1', '테스트1', 'test1@sk.com');
INSERT INTO USER (USER_ID, PASSWORD, NAME, EMAIL) VALUES('test2', '1', '테스트2', 'test2@sk.com');
INSERT INTO USER (USER_ID, PASSWORD, NAME, EMAIL) VALUES('test3', '1', '테스트3', 'test3@sk.com');
